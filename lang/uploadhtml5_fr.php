<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/uploadhtml5.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'case_charger_public' => 'Charger les scripts sur dans l’espace public',
	'case_remplacer_editer_logo' => 'Remplacer le formulaire d’upload de logo de SPIP',
	'cfg_titre_parametrages' => 'Paramétrages',
	'contain' => 'Réduire',
	'crop' => 'Recadrer',

	// D
	'drop_annuler' => 'Envoie annulé',
	'drop_annuler_confirm' => 'Annuler cet envoi ?',
	'drop_fallbacktext' => 'Merci d’utiliser ce formulaire à la place',
	'drop_fichier_invalide' => 'Type de fichier invalide',
	'drop_fichier_trop_gros' => 'Votre fichier est trop volumineux',
	'drop_ici' => 'Déposez vos fichiers ici ou cliquez sur ce cadre',
	'drop_max_file' => 'Nombre maximum de fichiers atteint',
	'drop_no_support' => 'Votre navigateur ne supporte pas le Glisser-Déposer',

	// E
	'explication_max_file' => 'Nombre de maximum de fichiers qui peuvent être uploadés simultanément (0 pour ne pas limiter).',
	'explication_max_file_size' => 'Taille maximum des fichiers (en MB). Il est conseillé d’utiliser la valeur du serveur (@uploadmaxsize@).',
	'explication_resizeMethod' => 'Méthode de réduction des images dans le cas ou la hauteur <strong>et</strong> la largeur sont forcées.',
	'explication_resizeWidth' => 'L’image va être réduite à cette dimention avant d’être envoyée. Si seulement une des valeurs est remplie, hauteur ou largeur, le ratio de l’image sera respecté.',

	// L
	'label_charger_public' => 'Script dans l’espace public',
	'label_remplacer_editer_logo' => 'Remplacer logo',
	'logo_drop_ici' => 'Déposez votre logo ici ou cliquez sur ce cadre',

	// M
	'max_file' => 'Nombre maximum de fichiers',
	'max_file_size' => 'Taille maximum des fichiers',

	// R
	'resizeHeight' => 'Recadrer/réduire en hauteur',
	'resizeMethod' => 'Méthode',
	'resizeQuality' => 'Qualité (en %)',
	'resizeWidth' => 'Recadrer/réduire en largeur',

	// T
	'titre_fieldset_image' => 'Modifier les images',
	'titre_page_configurer_uploadhtml5' => 'Configuration du formulaire d’upload html5',

	// U
	'uploadhtml5_titre' => 'Formulaire upload html5'
);
