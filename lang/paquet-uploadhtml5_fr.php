<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/uploadhtml5.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// U
	'uploadhtml5_description' => '',
	'uploadhtml5_nom' => 'Formulaire upload html5',
	'uploadhtml5_slogan' => 'Un formulaire d’upload en html5'
);
