<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-uploadhtml5?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// U
	'uploadhtml5_description' => '',
	'uploadhtml5_nom' => 'Uploadformular in HTML5',
	'uploadhtml5_slogan' => 'Ein Uploadformular in HTML5'
);
