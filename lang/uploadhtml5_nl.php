<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/uploadhtml5?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'case_charger_public' => 'De scripts in de openbare site laden',
	'case_remplacer_editer_logo' => 'Het logo uploadformulier van SPIP vervangen',
	'cfg_titre_parametrages' => 'Parameters',
	'contain' => 'Reduceren',
	'crop' => 'Herkaderen',

	// D
	'drop_annuler' => 'Verzending geannuleerd',
	'drop_annuler_confirm' => 'Deze verzending annuleren?',
	'drop_fallbacktext' => 'Gebruik in de plaats dit formulier',
	'drop_fichier_invalide' => 'Ongeldig bestandstype',
	'drop_fichier_trop_gros' => 'Dit bestand is te groot',
	'drop_ici' => 'Plaats je bestanden hier, of klik op dit kader',
	'drop_max_file' => 'Maximum aantal bestanden is bereikt',
	'drop_no_support' => 'Je browser ondersteunt geen verschuiven-plaatsen',

	// E
	'explication_max_file' => 'Maximum aantal bestanden dat tegelijkertijd kan worden geupload (0 betekent onbeperkt).',
	'explication_max_file_size' => 'Maximum bestandsgrootte (in Mb). Gebruik bij voorkeur de serverwaarde (@uploadmaxsize@).',
	'explication_resizeMethod' => 'Reductiemethode in geval dat de hoogte <strong>en</strong> de breedte geforceerd worden.',
	'explication_resizeWidth' => 'De afbeelding wordt voor het verzenden tot deze afmeting gereduceerd. Wanneer slechts één waarde wordt ingevuld (hoofte of breedte) zal de beeldverhouding worden aangehouden.',

	// L
	'label_charger_public' => 'Script in de openbare site',
	'label_remplacer_editer_logo' => 'Logo vervangen',
	'logo_drop_ici' => 'Plaats je logo hier, of klik op dit kader',

	// M
	'max_file' => 'Maximum aantal bestanden',
	'max_file_size' => 'Maximale bestandsgrootte',

	// R
	'resizeHeight' => 'Herkaderen/in hoogte reduceren',
	'resizeMethod' => 'Methode',
	'resizeQuality' => 'Kwaliteit (in %)',
	'resizeWidth' => 'Herkaderen/in breedte reduceren',

	// T
	'titre_fieldset_image' => 'Afbeeldingen aanpassen',
	'titre_page_configurer_uploadhtml5' => 'Configuratie van het html5 uploadformulier',

	// U
	'uploadhtml5_titre' => 'Formulier voor html5 upload'
);
