<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/uploadhtml5?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'case_charger_public' => 'Carregar os scripts na área pública',
	'case_remplacer_editer_logo' => 'Substituir o formulário de upload de logo do SPIP',
	'cfg_titre_parametrages' => 'Parâmetros',
	'contain' => 'Reduzir',
	'crop' => 'Reenquadrar',

	// D
	'drop_annuler' => 'Envio cancelado',
	'drop_annuler_confirm' => 'Cancelar este envio?',
	'drop_fallbacktext' => 'Por facor, use este formulário em substituicão',
	'drop_fichier_invalide' => 'Tipo de arquivo inválido',
	'drop_fichier_trop_gros' => 'O seu arquivo é muito grande',
	'drop_ici' => 'Solte os seus arquivos aqui, ou clique no box',
	'drop_max_file' => 'Número máximo de arquivos atingido',
	'drop_no_support' => 'O seu navegador não suporta Arrastar-Soltar',

	// E
	'explication_max_file' => 'Número máximo de arquivos que podem ser transferidos simultaneamente (0 para não limitar).',
	'explication_max_file_size' => 'Tamanhho máximo dos arquivos (em MB). É aconselhável usar o valor do servidor (@uploadmaxsize@).',
	'explication_resizeMethod' => 'Método de redução das imagens no caso da altura <strong>e</strong> da largura serem forçadas.',
	'explication_resizeWidth' => 'A imagem será reduzida a estas dimensões antes de ser enviada. Se apenas um desses valores for trocado, altura oou argura, a proporção da imagem será respeitada. ',

	// L
	'label_charger_public' => 'Script no espaço público',
	'label_remplacer_editer_logo' => 'Substituir logo',
	'logo_drop_ici' => 'Solte o seu logo aqui ou clique sobre o box',

	// M
	'max_file' => 'Número máximo de arquivos',
	'max_file_size' => 'Tamanho máximo dos arquivos',

	// R
	'resizeHeight' => 'Reenquadrar/reduzir a altura',
	'resizeMethod' => 'Método',
	'resizeQuality' => 'Qualidade (em %)',
	'resizeWidth' => 'Reenquadrar/reduzir a largura',

	// T
	'titre_fieldset_image' => 'Alterar as imagens',
	'titre_page_configurer_uploadhtml5' => 'Configuração do formulário de upload html5',

	// U
	'uploadhtml5_titre' => 'Formulário de upload html5'
);
