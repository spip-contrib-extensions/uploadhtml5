<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/uploadhtml5?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'case_charger_public' => 'Skripte des Formulars im öffentlichen Bereich laden',
	'cfg_titre_parametrages' => 'Parameter',

	// D
	'drop_annuler' => 'Upload abgebrochen',
	'drop_annuler_confirm' => 'Diesen Upload abbrechen ?',
	'drop_fallbacktext' => 'Bitte nutzen Sie dieses Formular',
	'drop_fichier_invalide' => 'Ungültiger Dateityp',
	'drop_fichier_trop_gros' => 'Ihre Datei ist zu gross',
	'drop_ici' => 'Legen Sie Ihre Dateien hier ab, oder klicken Sie in den Kasten',
	'drop_max_file' => 'Höchstzahl Dateien ist erreicht',
	'drop_no_support' => 'Ihr Browser unterstützt Ziehen und Ablegen nicht.',

	// E
	'explication_max_file' => 'Höchste Zahl der gleichzeitig übertragbaren Dateien (0 = keine Obergrenze).',
	'explication_max_file_size' => 'Maximale Dateigrösse in MB. Es empfiehlt sich, die Einstellungen des Servers zu verwenden (@uploadmaxsize@).', # MODIF

	// L
	'label_charger_public' => 'Öffentlich verfügbar',
	'logo_drop_ici' => 'Legen Sie Ihr Logo hier ab oder klicken Sie in den Kasten',

	// M
	'max_file' => 'Nombre maximum de fichiers',
	'max_file_size' => 'Maximale Dateigrösse',

	// T
	'titre_page_configurer_uploadhtml5' => 'HTML5-Uploadformular konfigurieren',

	// U
	'uploadhtml5_titre' => 'Uploadformular in HTML5'
);
