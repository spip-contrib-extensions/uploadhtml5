uploadhtml5
===========

![uploadhtml5](./prive/themes/spip/images/uploadhtml5-xx.svg)

Un formulaire d'upload HTML5 pour SPIP.

L'utilisation est simplifié au maximum:

```
#FORMULAIRE_UPLOADHTML5{objet, id_objet}
```

Ce formulaire va créer une "dropzone" et ajoutera les fichiers au portfolio de l'objet ciblé.

**Ce plugin n’est plus utile et n’est plus maintenu à partir de SPIP 4.0.0 qui intègre BigUp qui peut le remplacer.**


Documentation:\
https://contrib.spip.net/Formulaire-d-upload-en-html5