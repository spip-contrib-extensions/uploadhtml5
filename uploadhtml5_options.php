<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// par défaut, ne pas obfusquer les documents
if (!defined('_UPLOADHTML5_OBFUSQUER')) {
	define('_UPLOADHTML5_OBFUSQUER', false);
}
